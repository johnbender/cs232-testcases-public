//This program contains several class inheritances, and function calls on inherited classes. 
class Foo {
	public static void main(String[] s) {
		A a;
		B b;
		a = new A();
		b = new B();
		a = b;
		System.out.println(a.m(new S()));
	}
}

class A {
	public int m(Q q) {
	  int ret;
	  X x;
	  x = q.getX();
	  ret = x.n();
	  return ret;
	}
}

class B extends A {
	public int m(S s) {
		int ret;
		X x;
		x = s.getX();
		ret = x.n();
		return ret;
	}
}

class Q {
	X x;
	public X getX() {
		x = new X();
		return x;
	}
}

class S extends Q {
	X x;
	public X getX() {
		x = new X();
		return x;
	}
}

class X {
	public int n() {
		return 1;
	}
}
