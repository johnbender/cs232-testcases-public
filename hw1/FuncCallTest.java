/* 
 * Test multiple layers of function calls that are kind of recursive.
 */

class FuncCallTest {
    public static void main(String[] args){
        A a = new A();
        B b = new B();
        System.out.println(a.go(b));
    }
}

class A {
    public int go(B b) {
        return b.go(new A2());
    }
}

class A2 extends A {
    public int go(B b) {
        A a = new A();
        return a.go(b);
    }
}

class B {
    public int go(A a) {
        return a.go(new B2());
    }
}

class B2 extends B {
    public int go(A a) {
        return 232;
    } 
}

