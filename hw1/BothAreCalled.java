// At runtime, both method calls will call B_override. However, according
// to 0CFA, since a is assigned to the result of B_override (which returns
// a new A()), the call graph is conservative and has both A_override and
// B_override. This is because 0CFA is insensitive to the flow of the program.
class BothAreCalled {
    public static void main(String[] a) {
        System.out.println(new Run().run());
    }
}

class Run {
    public int run() {
        A a;
		B b;

        b = new B();
        a = b;

        a = a.override(a);
        a = b.override(b);

        return 0;
    }
}

class A {
    public A override(A in_a) {
    	return in_a;
    }
}

class B extends A {
	public A override(A in_b) {
		A b_a;
		b_a = new A();
		return b_a;
	}
}

