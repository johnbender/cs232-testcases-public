// This input program is nifty because it contains calls from both classes B and C 
// to the superclass A with the identity(int i) function. Additionally, for a bit 
// more of a challenge, this example ensures that subtype relationships of arguments
// must be accounted for as well. 

class SubtypeExample {
	public static void main(String[] args) {
		A a = new A();
		System.out.println(a.compute(new B()));
		System.out.println(a.compute(new C()));
	}
}

class A {
	public int compute(A arg) {
		return arg.compute(arg);
	}

	public int identity(int i) {
		return i;
	}
}

class B extends A {
	public int compute(A arg) {
		return this.identity(4);
	}	
}

class C extends B {
	public int compute(A arg) {
		return this.identity(3);
	}
}
