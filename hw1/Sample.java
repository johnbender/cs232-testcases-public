//this example if done via class heirarchy analysis the obj formal parameter is less precise than if done via 0-CFA.
class Sample {
    public static void main(String[] a){
        System.out.println(new A().compute(new B()));
        System.out.println(new A().compute(new C()));
    }
}

class A{
    int op;

    public int compute(A obj){
	int result;
        op = obj.init();
        result = this.mult(op, 2);
        return result;
    }

    public int init(){
        return 1;
    }

    public int mult(int a, int b){
        return a * b;
    }
}

class B extends A{
   public int add(int a , int b){
       return a+b;
   }
}

class C extends B{
    public int init(){
        return 2;
    }
}
