// This test case tests a recursive function call. It tests to see if we have already seen this edge in the call graph and stops looking further down.

class ComputeFibonacci {

    public static void main(String[] args) {
        int idx;
        int number;

        idx = 0;
        number = 10;

        while (idx < number) {
            idx = idx + 1;
            System.out.println(new Fibonacci().compute(idx));
        }
    }
}

class Fibonacci {

    public int compute(int number) {

        if (number < 3) {
            return 1;
        }
        return this.compute(number - 1) + this.compute(number - 2);
    }

}
