class Test {
    public static void main(String[] a){
        a = new A();
        b = new B();
        c = new C();
        b = a.m(b);
        c = b.m(c);
    }
}

class A {
	public B m(B b){
		return b.m(b);
	}
}

class B extends A{
	public B m(B b){
		b = new C();
		return b.m(b);
	}
}
class C extends B{
	public B m(B b){
		return new C();
	}
}

